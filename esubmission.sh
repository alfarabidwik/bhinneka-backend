#!/usr/bin/env bash
PROJECT_DIR="$(dirname $(dirname $(realpath $0)) )/bhinneka-backend"
SERVICE_NAME=esubmision
PATH_TO_JAR="$PROJECT_DIR/target/bhinneka-backend-0.0.1-SNAPSHOT.jar"
PID_PATH_NAME=/tmp/esubmission-pid

case $1 in
build)
       echo "Build Starting $SERVICE_NAME ..."
       nohup mvn clean install > ./esubmission.out 2>&1
       echo "Build Started $SERVICE_NAME ..."
;;
build-and-start)
       echo "Build & Starting $SERVICE_NAME ..."
       nohup mvn clean install > ./esubmission.out 2>&1
       echo $PATH_TO_JAR
  if [ ! -f $PID_PATH_NAME ]; then
       nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >>/dev/null &
                   echo $! > $PID_PATH_NAME
       echo "Build Started & $SERVICE_NAME service started ..."
  else
       echo "$SERVICE_NAME is already running ..."
  fi
;;
start)
       echo "Starting $SERVICE_NAME ..."
       echo $PATH_TO_JAR
  if [ ! -f $PID_PATH_NAME ]; then
       nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >>/dev/null &
                   echo $! > $PID_PATH_NAME
       echo "$SERVICE_NAME started ..."
  else
       echo "$SERVICE_NAME is already running ..."
  fi
;;
stop)
  if [ -f $PID_PATH_NAME ]; then
         PID=$(cat $PID_PATH_NAME);
         echo "$SERVICE_NAME stoping ..."
         kill $PID;
         echo "$SERVICE_NAME stopped ..."
         rm $PID_PATH_NAME
  else
      	 echo "$SERVICE_NAME is not running ..."
  fi
;;
restart)
  if [ -f $PID_PATH_NAME ]; then
      PID=$(cat $PID_PATH_NAME);
      echo "$SERVICE_NAME stopping ...";
      kill $PID;
      echo "$SERVICE_NAME stopped ...";
      rm $PID_PATH_NAME
      echo "$SERVICE_NAME starting ..."
      nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >> /dev/null &
      echo $! > $PID_PATH_NAME
      echo "$SERVICE_NAME started ..."
  else
      echo "$SERVICE_NAME is not running ..."
     fi     ;;
 esac
