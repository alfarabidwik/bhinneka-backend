package id.co.insura.bhinneka.security;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.utils.Constant;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${chicken.crispy}")
    private String chickenCrispy ;
    @Value("${chicken.crispy.validity.in.day}") int chickenCrispyValidityInDay ;
    @Value("${chicken.crispy.validity.in.minute}") int chickenCrispyValidityInMinute ;

    @Autowired
    protected MessageSource messageSource;
    @Autowired Gson gson ;

    private long aDayTimeMilis = 1000*60*60*24; //3600000*24*30*12; // 1h
    private long aMinuteTimeMIlis = 1000*60;


    public String createToken(String username, JLogin jLogin) {

        Claims claims = Jwts.claims().setSubject(username);
        claims.put(Constant.J_LOGIN, gson.toJson(jLogin));

        Date now = new Date();
        Date validity = new Date(now.getTime() + (aMinuteTimeMIlis*chickenCrispyValidityInMinute));
        claims.put("expired", validity);

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)//
            .signWith(SignatureAlgorithm.HS256, chickenCrispy)//
            .compact();
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return bearerToken;
    }


    public JLogin getUser(String authorization, boolean validate){
        if(validate){
            validateAuthorization(authorization);
        }

//        Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(chickenCrispy).parseClaimsJws(authorization);//.getBody();//.get("roles", ArrayList.class);
//        Claims claims = jwt.getBody();
//        Type type = new TypeToken<Map>(){}.getType();
//        Map map = gson.fromJson(gson.toJson(claims), type);
        Map map = getClaim(authorization);
        List<String> roles = (List<String>) map.get("roles");
        String coreLoginString = (String) map.get(Constant.J_LOGIN);
        JLogin jLogin = gson.fromJson(coreLoginString, JLogin.class);
        return jLogin ;
    }


    public Map getClaim(String authorization){
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(chickenCrispy).parseClaimsJws(authorization);//.getBody();//.get("roles", ArrayList.class);
            Claims claims = jwt.getBody();
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            return map ;
        }catch (ExpiredJwtException e){
            throw new AppException(HttpStatus.UNAUTHORIZED, "token.has.expired");
        }catch (Exception e){
            throw new AppException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    public boolean validateAuthorization(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(chickenCrispy).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())) {
                Locale locale = LocaleContextHolder.getLocale();
                throw new AppException(HttpStatus.UNAUTHORIZED, messageSource.getMessage("authorization.is.expired", null, locale));
            }
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            if(e instanceof ExpiredJwtException){
                throw new AppException(HttpStatus.UNAUTHORIZED, "token.has.expired");
            }
            throw new AppException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}