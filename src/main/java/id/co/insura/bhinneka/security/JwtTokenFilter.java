package id.co.insura.bhinneka.security;

import id.co.insura.bhinneka.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends GenericFilterBean {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
        throws IOException, ServletException, AppException {

//        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
//        List<String> roles = jwtTokenProvider.getRoles(token);
//        logger.debug("roles {}", roles);
//        if(roles==null || roles.size()<=0){
//            filterChain.doFilter(req, res);
//        }else{
//            String role = roles.get(0);
//            Authentication auth = jwtTokenProvider.getUserAuthentication(token);
//            if (auth != null) {
//                SecurityContextHolder.getContext().setAuthentication(auth);
//            }
//        }
//        String role = ((HttpServletRequest) req).getHeader(Constant.ROLE);
        filterChain.doFilter(req, res);
    }


}