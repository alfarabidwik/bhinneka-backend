package id.co.insura.bhinneka.controller;

import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.model.MstStatusDto;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.service.MstStatusService;
import id.co.insura.bhinneka.utils.Constant;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "${api}")
public class MstStatusController extends BaseController<MstStatus> {

    @Autowired
    MstStatusService mstStatusService ;

    @Override
    public MstStatusService mainService() {
        return mstStatusService;
    }

    @GetMapping(path = "/mstStatuses")
    @Override
    public WSResponse findAll(@ApiParam(allowableValues = "true,false", required = true) @RequestParam(defaultValue = "true") Boolean ascending,
                              @ApiParam(allowableValues = "status, comment, created", required = true) @RequestParam(defaultValue = "status") String sortir,
                              @RequestParam(required = false) Integer page) {
        return super.findAll(ascending, sortir, page);
    }

    @GetMapping(path = "/mstStatus")
    @Override
    public WSResponse findById(@RequestParam Long id) {
        return super.findById(id);
    }

    @GetMapping(path = "/mstStatus/delete")
    @Override
    public WSResponse delete(@RequestParam Long id) {
        return super.delete(id);
    }

    @PostMapping(path = "/mstStatus/save")
    public WSResponse save(@RequestBody MstStatusDto dto) {
        MstStatus entity =  modelMapper.map(dto, MstStatus.class);
        entity =  mainService().save(entity);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, entity);
    }
}
