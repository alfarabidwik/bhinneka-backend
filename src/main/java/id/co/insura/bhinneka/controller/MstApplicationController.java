package id.co.insura.bhinneka.controller;

import id.co.insura.bhinneka.annotations.SwagEnable;
import id.co.insura.bhinneka.entity.MstApplication;
import id.co.insura.bhinneka.model.MstApplicationDto;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.service.MstApplicationService;
import id.co.insura.bhinneka.utils.Constant;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "${api}")
public class MstApplicationController extends BaseController<MstApplication> {

    @Autowired
    MstApplicationService mstApplicationService ;

    @Override
    public MstApplicationService mainService() {
        return mstApplicationService;
    }

    @GetMapping(path = "/mstApplications")
    @Override
    public WSResponse findAll(@ApiParam(allowableValues = "true,false", required = true) @RequestParam(defaultValue = "true") Boolean ascending,
                              @ApiParam(allowableValues = "created", required = true) @RequestParam(defaultValue = "created") String sortir,
                              @RequestParam(required = false) Integer page) {
        return super.findAll(ascending, sortir, page);
    }

    @GetMapping(path = "/mstApplication")
    @Override
    public WSResponse findById(@RequestParam Long id) {
        return super.findById(id);
    }

    @GetMapping(path = "/mstApplication/delete")
    @Override
    public WSResponse delete(@RequestParam Long id) {
        return super.delete(id);
    }

    @PostMapping(path = "/mstApplication/save")
    public WSResponse save(@RequestBody MstApplicationDto dto) {
        MstApplication entity =  modelMapper.map(dto, MstApplication.class);
        entity =  mainService().save(entity);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, entity);
    }

    @GetMapping(path = "/mstApplication/activeVersion")
    @SwagEnable
    public WSResponse activeVersion() {
        MstApplication mstApplication =  mainService().findTopByVersionDesc().orElse(null);
        if(mstApplication==null){
            return WSResponse.instance(Constant.SUCCESS_CODE, "no.available.update.found");
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.FETCHING_SUCCESS, mstApplication);
    }

}
