package id.co.insura.bhinneka.controller;

import id.co.insura.bhinneka.annotations.SwagEnable;
import id.co.insura.bhinneka.entity.MstApplication;
import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.entity.TrxUploadFileLog;
import id.co.insura.bhinneka.entity.esub.TrxSubmission;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.model.core.esub.Esubmission;
import id.co.insura.bhinneka.model.core.esub.UploadFileResponse;
import id.co.insura.bhinneka.model.misc.DataPage;
import id.co.insura.bhinneka.service.MstApplicationService;
import id.co.insura.bhinneka.service.UploadFileService;
import id.co.insura.bhinneka.service.esub.TrxSubmissionService;
import id.co.insura.bhinneka.utils.AppRestTemplate;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.MediaTypeUtils;
import id.co.insura.bhinneka.utils.Utils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MimeType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(path = "${api}")
public class TrxSubmissionController extends BaseController<TrxSubmission> {
    Logger logger  = LoggerFactory.getLogger(TrxSubmissionController.class);

    @Autowired TrxSubmissionService trxSubmissionService ;
    @Autowired MstApplicationService mstApplicationService ;
    @Autowired UploadFileService uploadFileService ;

    @Autowired ServletContext servletContext;


    @Value("${upload.blob.core.address}") String uploadBlobCoreAddress ;

    @Autowired
    protected AppRestTemplate restTemplate;


    @Override
    public TrxSubmissionService mainService() {
        return trxSubmissionService;
    }

    @GetMapping(path = "/trxSubmissions")
    public WSResponse findAll(
            @RequestParam(required = false) String agentCode,
            @ApiParam(allowableValues =
                            MstStatus.SUBMITTED +","+
                            MstStatus.INACTIVE_AGENT+","+
                            MstStatus.IDCARD_REJECTED+","+
                            MstStatus.INCOMPLETE_AGENT_CORE+","+
                            MstStatus.LICENSE_EXPIRED+","+
                            MstStatus.PAYMENT_INVALID+","+
                            MstStatus.PAYMENT_VALIDATION+","+
                            MstStatus.PAID)
            @RequestParam(required = false) String status,
            @ApiParam(allowableValues = "true,false", required = true) @RequestParam(defaultValue = "true") Boolean ascending,
            @ApiParam(allowableValues = "spajNo, noVa, version, agentCode, agentName, agentName, officeName, created", required = true)
            @RequestParam(defaultValue = "status") String sortir,
            @RequestParam(required = false) Integer page) {
        if(agentCode!=null || status!=null){
            DataPage<TrxSubmission> dataPage = mainService().trxSubmissionDataPage(agentCode, status, ascending, sortir, page);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, dataPage.getDatas()).setTotalPage(dataPage.getTotalPage())
                    .setTotalElement(dataPage.getTotalElement()).setPageElement(dataPage.getPageElement());
        }
        return super.findAll(ascending, sortir, page);
    }

    @GetMapping(path = "/trxSubmission")
    @Override
    public WSResponse findById(@RequestParam Long id) {
        return super.findById(id);
    }

    @GetMapping(path = "/trxSubmission/delete")
    @Override
    public WSResponse delete(@RequestParam Long id) {
        return super.delete(id);
    }

    @PostMapping(path = "/trxSubmission/save", consumes = { "multipart/form-data" })
    @SwagEnable
    public WSResponse save(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
            @ApiParam(required = true) @RequestParam String spajNo,
            @ApiParam(required = true) @RequestParam String noVa,
            @ApiParam(required = true) @RequestParam Integer version,
            @ApiParam(required = true) @RequestParam String groupPolicyId,
            @ApiParam(required = true) @RequestParam String agentCode,
            @ApiParam(required = true) @RequestParam String agentName,
            @ApiParam(required = true) @RequestParam String officeName,
            @ApiParam(required = true) @RequestPart String json,
            @ApiParam(required = true) @RequestPart String spajPdfBase64,
            @ApiParam(required = true) @RequestPart String idCardBase64) throws Exception{

        if(StringUtils.isEmpty(spajPdfBase64)){
            throw new AppException(HttpStatus.BAD_REQUEST, message("SPAJ file cannot being empty"));
        }
        if(StringUtils.isEmpty(idCardBase64)){
            throw new AppException(HttpStatus.BAD_REQUEST, message("SPAJ file cannot being empty"));
        }


        jwtTokenProvider.validateAuthorization(authorization);

        MstApplication mstApplication =  mstApplicationService.findTopByVersionDesc().orElse(null);
        if(mstApplication!=null){
            if(mstApplication.getVersion()>version){
                throw new ResponseStatusException(HttpStatus.UPGRADE_REQUIRED, "must.to.update.an.apps");
            }
        }

        TrxSubmission trxSubmission = new TrxSubmission();
        trxSubmission.setSpajNo(spajNo);
        trxSubmission.setNoVa(noVa);
        trxSubmission.setVersion(version);
        trxSubmission.setGroupPolicyId(groupPolicyId);
        trxSubmission.setAgentCode(agentCode);
        trxSubmission.setAgentName(agentName);
        trxSubmission.setOfficeName(officeName);
        trxSubmission.setJson(json);
        trxSubmission.initTransient();

        File spajPdfFile = base64ToFile(spajPdfBase64);
        File idCardFile = base64ToFile(idCardBase64);

        trxSubmission =  mainService().submitPost(authorization, trxSubmission, spajPdfFile, idCardFile);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, trxSubmission);
    }

    @PostMapping(path = "/trxSubmission/payment", consumes = { "multipart/form-data" })
    @SwagEnable
    @ApiOperation(notes = "1 = TRANSFER\n2 = GOPAY", value = "Please see a payment method option below")
    public WSResponse payment(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
            @ApiParam(required = true) @RequestParam Long esubId,
            @ApiParam(allowableValues = "1,2", required = true, defaultValue = "2") @RequestParam Integer paymentMethod,
            @ApiParam(required = false) @RequestParam(required = false) String kodeSetor,
            @ApiParam(required = false) @RequestParam(required = false) String attachmentBase64) throws Exception{

        jwtTokenProvider.validateAuthorization(authorization);
        File file =  base64ToFile(attachmentBase64);

        TrxSubmission trxSubmission = trxSubmissionService.paymentPost(authorization, esubId, paymentMethod, kodeSetor, file);

        return WSResponse.instance(Constant.SUCCESS_CODE, trxSubmission);

    }

//    @PostMapping(path = "/trxSubmission/testBase64", consumes = { "multipart/form-data" })
//    @SwagEnable
//    public ResponseEntity testBase64(@RequestPart String base64) throws Exception{
//        File file = base64ToFile(base64);
//        return grabFile(file.getName(), file.getAbsolutePath());
//    }
//
//    @Cacheable(cacheNames = "images", key = "{#pathFile, #filename}")
//    public ResponseEntity<ByteArrayResource> grabFile(String filename, String pathFile) throws Exception{
//        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, filename);
//        Path path = Paths.get(pathFile);
//        byte[] data = Files.readAllBytes(path);
//        ByteArrayResource resource = new ByteArrayResource(data);
//
//
//        return ResponseEntity.ok()
//                // Content-Disposition
//                .cacheControl(CacheControl.maxAge(360, TimeUnit.DAYS).cachePublic())
//                .header(com.google.common.net.HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
//                // Content-Type
//                .contentType(mediaType) //
//                // Content-Lengh
//                .contentLength(data.length) //
//                .body(resource);
//    }



}
