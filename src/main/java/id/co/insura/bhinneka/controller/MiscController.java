package id.co.insura.bhinneka.controller;

import com.google.gson.JsonSyntaxException;
import id.co.insura.bhinneka.annotations.SwagEnable;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.AesEncryptor;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${api}")
public class MiscController extends BaseController {

    Logger logger = LoggerFactory.getLogger(MiscController.class);

    @Autowired AesEncryptor aesEncryptor;


//    @GetMapping("/encrypt")
//    public WSResponse encrypt(@RequestParam String value){
//        try{
//            String encrypt = rsaEncryptor.encrypt(value);
//            encrypt = URLEncoder.encode(encrypt, "UTF-8");
//            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, encrypt);
//        }catch (Exception e){
//            e.printStackTrace();
//            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
//        }
//    }

//    @GetMapping("/decrypt")
//    public WSResponse decrypt(String value){
//        try{
//            logger.debug("DECODED URL ### {} ", value);
//            String decrypt = aesEncryptor.decrypt(value);
//            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, decrypt);
//        }catch (Exception e){
//            e.printStackTrace();
//            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
//        }
//    }

    @GetMapping("/test")
    public WSResponse test(){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    @Override
    public BaseService mainService() {
        return null;
    }
}
