package id.co.insura.bhinneka.controller.login;

import id.co.insura.bhinneka.annotations.SwagEnable;
import id.co.insura.bhinneka.controller.BaseController;
import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.security.JwtTokenProvider;
import id.co.insura.bhinneka.service.login.JLoginService;
import id.co.insura.bhinneka.utils.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping(path = "${api}")
public class AuthController extends BaseController<JLogin> {

    Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    JLoginService jLoginService ;
    @Autowired JwtTokenProvider jwtTokenProvider ;

    @Override
    public JLoginService mainService() {
        return jLoginService;
    }


    @PostMapping("/login")
    @SwagEnable
    public WSResponse login(String username,
                            String password,
                            String platform,
                            String imei,
                            Integer applicationVersion) throws AppException{
        JLogin jLogin = jLoginService.login(username, password, platform, imei, applicationVersion);
        String authorization = jwtTokenProvider.createToken(username, jLogin);
        jLogin.setAuthorization(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, authorization, jLogin);

    }

    @PostMapping("/logout")
    @SwagEnable
    public WSResponse logout(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization){
        JLogin jLogin = jwtTokenProvider.getUser(authorization, false);
        jLoginService.delete(jLogin.getId());
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);

    }


    @GetMapping("/generateAuthorization")
    @SwagEnable
    public WSResponse generateAuthorization(){
        JLogin jLogin = new JLogin();
        jLogin.setUsername("Test");
        jLogin.setPlatform("Test");
        jLogin.setImei("12345678");
        jLogin.setApplicationVersion(1);
        String authorization = jwtTokenProvider.createToken(jLogin.getUsername(), jLogin);
        jLogin.setAuthorization(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, authorization);
    }

    @GetMapping("/checkAuth")
    @SwagEnable
    public WSResponse checkAuth(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization){
        Map map = jwtTokenProvider.getClaim(authorization);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(((Double) map.get("expired")).longValue());
        map.put("expiredDate",calendar.getTime());
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, map);
    }



}
