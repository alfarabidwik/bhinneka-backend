package id.co.insura.bhinneka.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import id.co.insura.bhinneka.entity.BasicField;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.WSResponse;
import id.co.insura.bhinneka.security.JwtTokenProvider;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.ValidationBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.MappedSuperclass;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import java.io.File;
import java.lang.reflect.Type;
import java.util.*;

@MappedSuperclass
@RestControllerAdvice
public abstract class BaseController<ENTITY extends BasicField>  extends ResponseEntityExceptionHandler{

    private static final Logger logger = LoggerFactory.getLogger(BaseController.class.getName());

    public abstract <T extends BaseService> T mainService();

    @Autowired MessageSource messageSource ;
    @Autowired
    protected Gson gson ;

    @Autowired
    protected ModelMapper modelMapper ;
    @Autowired
    LocalValidatorFactoryBean validator ;
    @Autowired
    protected HttpServletRequest request ;
    @Autowired
    protected JwtTokenProvider jwtTokenProvider ;


    @Value("${page.row}")
    protected int pageRow ;

    public Type type(){
        return null ;
    }

    public Type typeList(){
        return null ;
    }

    public WSResponse findAll(Boolean ascending, String sortir, Integer page){
        Sort sort  = Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(page!=null){
            Page<ENTITY> entityPage = mainService().findAll(PageRequest.of(page, pageRow, sort));
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.FETCHING_SUCCESS, entityPage.getContent()).setPageElement(pageRow).setTotalElement(entityPage.getTotalElements())
                    .setTotalPage(entityPage.getTotalPages());
        }else{
            List<ENTITY> entities = new ArrayList<>();
            entities  = mainService().findAll(sort);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.FETCHING_SUCCESS, entities);
        }
    }

    public WSResponse findById(Long id){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.FETCHING_SUCCESS, mainService().findById(id));
    }

//    public <DTO extends EBaseDto> WSResponse save(DTO dto, Class entityClass){
//        ENTITY entity = (ENTITY) modelMapper.map(dto, entityClass);
//        entity = (ENTITY) mainService().save(entity);
//        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, entity);
//    }

    public WSResponse save(ENTITY entity){
        entity = (ENTITY) mainService().save(entity);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, entity);
    }

    public WSResponse save(String jsonEntity, Class clazz){
        ENTITY entity = (ENTITY) gson.fromJson(jsonEntity, clazz);
        entity = (ENTITY) mainService().save(entity);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SAVING_SUCCESS, entity);
    }

    public WSResponse delete(Long id){
        mainService().delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETING_SUCCESS);
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public <T> void validate(T object){
        Set<ConstraintViolation<T>> constrains = validator.validate(object);
        List<String> strings = new ArrayList<>();
        for (ConstraintViolation<T> constrain : constrains) {
            String objectError = constrain.getMessage();
            strings.add(objectError);
        }
        if(strings.size()>0){
            throw new AppException(HttpStatus.UNPROCESSABLE_ENTITY, ArrayUtils.toString(strings));
        }
    }

    public File base64ToFile(String base64) throws AppException{
        try{
            if(StringUtils.isEmpty(base64)){
                return null ;
            }
            String firstFlag = base64.split(";", 2)[0];
            String mimeType = firstFlag.split(":", 2)[1];
            String ext = mimeType.split("/", 2)[1];

            String[] base64s = base64.split(",", 2);
            String clearBase64 = "";
            if(base64s.length>1){
                clearBase64 = base64s[1];
            }else{
                clearBase64 = base64;
            }
            byte[] decodedBytes = Base64.getDecoder().decode(clearBase64);
            File file = new File(String.valueOf(System.nanoTime())+"."+ext);
            FileUtils.writeByteArrayToFile(file, decodedBytes);
            return file ;
        }catch (Exception e){
            e.printStackTrace();
            throw new AppException(HttpStatus.BAD_REQUEST, message("string.base.64.must.contain.opening.flag"));
        }
    }


    @ExceptionHandler(value = JsonSyntaxException.class)
    public ResponseEntity jsonSyntaxException(HttpServletRequest req, JsonSyntaxException e){
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(WSResponse.instance(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator")));
    }

    @ExceptionHandler(value = IllegalStateException.class)
    public ResponseEntity illegalStateException(HttpServletRequest req, IllegalStateException e){
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(WSResponse.instance(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator")));
    }

//    @ExceptionHandler(value = RuntimeException.class)
//    public ResponseEntity defaultErrorHandler(HttpServletRequest req, RuntimeException e) {
//        e.printStackTrace();
//        logger.error(req.getRequestURI(), e);
//        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(WSResponse.instance(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator")));
//    }

//    @ExceptionHandler(value = Exception.class)
//    public ResponseEntity defaultErrorHandler(HttpServletRequest req, Exception e){
//        e.printStackTrace();
//        logger.error(req.getRequestURI(), e);
//        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(WSResponse.instance(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator")));
//    }

//    @ExceptionHandler(value = AppException.class)
//    public WSResponse defaultErrorHandler(HttpServletRequest req, AppException e){
//        e.printStackTrace();
//        logger.error(req.getRequestURI(), e);
//        return WSResponse.instance(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
//    }





}
