package id.co.insura.bhinneka.model.core;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

public class CoreResponse<DATA> {

    @Getter@Setter
    int code ;

    @Getter@Setter
    Object status ;

    @Getter@Setter
    String name ;

    @Setter
    String message ;

    @Getter@Setter
    DATA data ;


    public String getMessage() {
        if(StringUtils.isEmpty(message) && StringUtils.isNotEmpty(name)){
            this.message = name ;
        }
        if(StringUtils.isNotEmpty(message) && StringUtils.isEmpty(name)){
            this.name = message ;
        }
        return message;
    }
}
