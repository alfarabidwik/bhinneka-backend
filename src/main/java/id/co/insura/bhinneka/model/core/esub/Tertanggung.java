package id.co.insura.bhinneka.model.core.esub;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.SerializedName;
import id.co.insura.bhinneka.utils.JsonDateDeserializer;
import id.co.insura.bhinneka.utils.JsonDateSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class Tertanggung {

    int no ;

    @JsonProperty("nama")
    @SerializedName("nama")
    String nama ;

    @JsonProperty("tgl_lahir")
    @SerializedName("tgl_lahir")
    String tglLahir ;

    @JsonProperty("hubungan")
    @SerializedName("hubungan")
    String hubungan ;

}
