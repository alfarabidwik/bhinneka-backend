package id.co.insura.bhinneka.model.core.login;

import lombok.Data;

import java.util.Map;

@Data
public class CoreLogin {

//    {
//        "key": "string",
//            "login": "string",
//            "password": "string",
//            "expiry": 0,
//            "param": {},
//        "token2fa": "string",
//            "captcha": "string",
//            "forced": true
//    }

    String key ;
    String login ;
    String password ;
    int expiry ;
    Map param  ;
    String token2fa;
    String captcha ;
    boolean forced = true ;

}
