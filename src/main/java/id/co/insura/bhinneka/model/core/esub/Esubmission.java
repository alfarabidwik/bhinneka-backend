package id.co.insura.bhinneka.model.core.esub;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Esubmission implements Serializable {

    @JsonProperty("no_ktp")
    @SerializedName("no_ktp")
    @Getter String noKtp ;

    public void setNoKtp(String noKtp) {
        noKtp = StringUtils.trim(noKtp);
        this.noKtp = noKtp;
    }

    @JsonProperty("nama_pempol")
    @SerializedName("nama_pempol")
    @Getter String namaPempol ;

    public void setNamaPempol(String namaPempol) {
        namaPempol = StringUtils.trim(namaPempol);
        this.namaPempol = namaPempol;
    }

    @JsonProperty("produk")
    @SerializedName("produk")
    @Getter String produk ;

    public void setProduk(String produk) {
        produk = StringUtils.trim(produk);
        this.produk = produk;
    }

    @JsonProperty("pilihan_paket")
    @SerializedName("pilihan_paket")
    @Getter String pilihanPaket ;

    public void setPilihanPaket(String pilihanPaket) {
        pilihanPaket = StringUtils.trim(pilihanPaket);
        this.pilihanPaket = pilihanPaket;
    }

    @JsonProperty("alamat")
    @SerializedName("alamat")
    @Getter String alamat ;

    public void setAlamat(String alamat) {
        alamat = StringUtils.trim(alamat);
        this.alamat = alamat;
    }

    @JsonProperty("propinsi")
    @SerializedName("propinsi")
    @Getter String propinsi ;

    public void setPropinsi(String propinsi) {
        propinsi = StringUtils.trim(propinsi);
        this.propinsi = propinsi;
    }

    @JsonProperty("kota")
    @SerializedName("kota")
    @Getter String kota ;

    public void setKota(String kota) {
        kota = StringUtils.trim(kota);
        this.kota = kota;
    }

    @JsonProperty("kecamatan")
    @SerializedName("kecamatan")
    @Getter String kecamatan ;

    public void setKecamatan(String kecamatan) {
        kecamatan = StringUtils.trim(kecamatan);
        this.kecamatan = kecamatan;
    }

    @JsonProperty("kelurahan")
    @SerializedName("kelurahan")
    @Getter
    String kelurahan ;

    public void setKelurahan(String kelurahan) {
        kelurahan = StringUtils.trim(kelurahan);
        this.kelurahan = kelurahan;
    }

    @JsonProperty("kode_pos")
    @SerializedName("kode_pos")
    @Getter
    String kodePos ;

    @JsonProperty("no_agen")
    @SerializedName("no_agen")
    @Getter
    String noAgen ;

    public void setNoAgen(String noAgen) {
        this.noAgen = StringUtils.trim(noAgen);
    }

    @JsonProperty("no_hp")
    @SerializedName("no_hp")
    @Getter
    String noHp ;

    public void setNoHp(String noHp) {
        this.noHp = StringUtils.trim(noHp);
    }

    @JsonProperty("metode_pembayaran")
    @SerializedName("metode_pembayaran")
    @Getter
    String metodePembayaran ;

    public void setMetodePembayaran(String metodePembayaran) {
        this.metodePembayaran = StringUtils.trim(metodePembayaran);
    }

    @JsonProperty("kode_emoney")
    @SerializedName("kode_emoney")
    @Getter
    String kodeEmoney ;

    public void setKodeEmoney(String kodeEmoney) {
        this.kodeEmoney = StringUtils.trim(kodeEmoney);
    }

    @JsonProperty("file_ktp")
    @SerializedName("file_ktp")
    @Getter
    String fileKtp ;

    public void setFileKtp(String fileKtp) {
        this.fileKtp = StringUtils.trim(fileKtp);
    }

    @JsonProperty("file_spaj")
    @SerializedName("file_spaj")
    @Getter
    String fileSpaj ;

    public void setFileSpaj(String fileSpaj) {
        this.fileSpaj = StringUtils.trim(fileSpaj);
    }

    @JsonProperty("file_scan_pembayaran")
    @SerializedName("file_scan_pembayaran")
    @Getter
    String fileScanPembayaran ;

    public void setFileScanPembayaran(String fileScanPembayaran) {
        this.fileScanPembayaran = StringUtils.trim(fileScanPembayaran);
    }

    @JsonProperty("pilihan_cetak")
    @SerializedName("pilihan_cetak")
    @Getter
    String pilihanCetak ;

    public void setPilihanCetak(String pilihanCetak) {
        this.pilihanCetak = StringUtils.trim(pilihanCetak);
    }

    @JsonProperty("epolicy_email")
    @SerializedName("epolicy_email")
    @Getter
    String epolicyEmail ;

    public void setEpolicyEmail(String epolicyEmail) {
        this.epolicyEmail = StringUtils.trim(epolicyEmail);
    }

    @JsonProperty("epolicy_sms")
    @SerializedName("epolicy_sms")
    @Getter
    String epolicySms ;

    public void setEpolicySms(String epolicySms) {
        this.epolicySms = StringUtils.trim(epolicySms);
    }

    @JsonProperty("epolicy_wa")
    @SerializedName("epolicy_wa")
    @Getter
    String epolicyWa ;

    public void setEpolicyWa(String epolicyWa) {
        this.epolicyWa = StringUtils.trim(epolicyWa);
    }

    @JsonProperty("tertanggung")
    @SerializedName("tertanggung")
    @Getter@Setter
    List<Tertanggung> tertanggungList = new ArrayList<>();

    @JsonProperty("blobs")
    @SerializedName("blobs")
    @Getter@Setter
    List<UploadFileResponse> blobs = new ArrayList<>();

    /**
     * TODO Check Field below whether need or not
     * */
    @JsonProperty("kd_ukerja")
    @SerializedName("kd_ukerja")
    @Getter
    String kdUkerja ;

    public void setKdUkerja(String kdUkerja) {
        this.kdUkerja = StringUtils.trim(kdUkerja);
    }

    @JsonProperty("nohp_polis")
    @SerializedName("nohp_polis")
    @Getter
    String noHpPolis ;

    public void setNoHpPolis(String noHpPolis) {
        this.noHpPolis = StringUtils.trim(noHpPolis);
    }

    @JsonProperty("nama")
    @SerializedName("nama")
    @Getter
    String nama ;

    public void setNama(String nama) {
        this.nama = StringUtils.trim(nama);
    }

    @JsonProperty("tgl_pengajuan")
    @SerializedName("tgl_pengajuan")
    @Getter
    String tglPengajuan ;

    public void setTglPengajuan(String tglPengajuan) {
        this.tglPengajuan = StringUtils.trim(tglPengajuan);
    }

    @JsonProperty("premi")
    @SerializedName("premi")
    @Getter
    String premi ;

    public void setPremi(String premi) {
        this.premi = StringUtils.trim(premi);
    }

    @JsonProperty("status")
    @SerializedName("status")
    @Getter
    String status ;

    public void setStatus(String status) {
        this.status = StringUtils.trim(status);
    }

    @JsonProperty("penghasilan_per_tahun")
    @SerializedName("penghasilan_per_tahun")
    @Getter
    String penghasilanPerTahun ;

    public void setPenghasilanPerTahun(String penghasilanPerTahun) {
        this.penghasilanPerTahun = StringUtils.trim(penghasilanPerTahun);
    }

    @JsonProperty("pekerjaan")
    @SerializedName("pekerjaan")
    @Getter
    String pekerjaan ;

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = StringUtils.trim(pekerjaan);
    }

    @JsonProperty("cetak")
    @SerializedName("cetak")
    @Getter@Setter
    boolean cetak ;



}
