package id.co.insura.bhinneka.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class MstApplicationDto extends EBaseDto{
//    SELECT id, version, api, platform, app_version, status, start_date, cutoff_date, expiry_date, message, url
//    FROM LIFEPOS.dbo.MST_APPLICATION;

    int version ;

    int api ;

    String platform ;

    String appVersion ;

    String status ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date startDate ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date cutOffDate ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date expiryDate ;

    String message ;

    String url ;

}
