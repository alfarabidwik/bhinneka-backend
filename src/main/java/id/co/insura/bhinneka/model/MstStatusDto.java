package id.co.insura.bhinneka.model;

import lombok.Data;

@Data
public class MstStatusDto extends EBaseDto{

    private String status ;
    private String comment ;

}
