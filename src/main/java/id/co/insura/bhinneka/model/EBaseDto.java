package id.co.insura.bhinneka.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;

@Data
public abstract class EBaseDto implements Serializable {
    public static final Logger logger = LoggerFactory.getLogger(EBaseDto.class);

    protected Long id;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date created ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Long createdBy ;

    protected Date updated ;

    protected Long updatedBy ;

}