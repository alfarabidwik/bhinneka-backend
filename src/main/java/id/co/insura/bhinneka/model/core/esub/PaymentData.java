package id.co.insura.bhinneka.model.core.esub;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class PaymentData implements Serializable {

    public static final int TRANSFER = 1;
    public static final int GOPAY = 2;

    @JsonProperty("no_ktp")
    @SerializedName("no_ktp")
    private String noKtp ;

    @JsonProperty("kode_setor")
    @SerializedName("kode_setor")
    private String kodeSetor ;

    @JsonProperty("metode_pembayaran")
    @SerializedName("metode_pembayaran")
    private int metodePembayaran ;

    @JsonProperty("payment_date")
    @SerializedName("payment_date")
    private Date paymentDate ;

    @JsonProperty("blobs")
    @SerializedName("blobs")
    List<UploadFileResponse> blobs = new ArrayList<>();


    public PaymentData(){
        if(paymentDate==null){
            paymentDate = new Date();
        }
    }


}
