package id.co.insura.bhinneka.model.core.esub;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class UploadFileResponse {

    String container ;
    String subFolder ;
    String originalName ;
    String blobName ;
    String type ;
    String level ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date timestamp ;

    String bloburl ;


}
