package id.co.insura.bhinneka.model.core.login;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class CoreLoginResponse {

//    int seq ;
//    String token ;
//    String refresh ;
//    String sig ;
//    Map user = new HashMap();
	String code;
	String status;
//	String message;
	Map data = new HashMap();

}
