package id.co.insura.bhinneka.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class WSResponse<D, E> {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 0)
	@Getter @Setter Date timestamp ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 1)
	@Getter @Setter Integer status ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 2)
	@Getter@Setter String message ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 3)
	@Getter@Setter E error ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 4)
	@Getter@Setter D data ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 5)
	@Getter Long totalElement ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 6)
	@Getter Integer totalPage ;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@ApiModelProperty(position = 7)
	@Getter Integer pageElement ;


	public static <D> WSResponse instance(int status, String message, D data){
		WSResponse instance = new WSResponse<>();
		instance.setTimestamp(new Date());
		instance.setStatus(status);
		instance.setMessage(message);
		instance.setData(data);
		return instance ;
	}

	public static <D> WSResponse instance(int status, String message){
		WSResponse instance = new WSResponse<>();
		instance.setTimestamp(new Date());
		instance.setStatus(status);
		instance.setMessage(message);
		return instance ;
	}

	public static <E> WSResponse instanceError(int status, String message, E error){
		WSResponse instance = new WSResponse<>();
		instance.setTimestamp(new Date());
		instance.setStatus(status);
		instance.setMessage(message);
		instance.setError(error);
		return instance ;
	}

	public static <D> WSResponse instance(int status, D data){
		WSResponse instance = new WSResponse<>();
		instance.setTimestamp(new Date());
		instance.setStatus(status);
		instance.setData(data);
		return instance ;
	}

	public WSResponse setTotalElement(long totalElement) {
		this.totalElement = totalElement;
		return this ;
	}

	public WSResponse setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
		return this;
	}

	public WSResponse setPageElement(Integer pageElement) {
		this.pageElement = pageElement;
		return this ;
	}
}
