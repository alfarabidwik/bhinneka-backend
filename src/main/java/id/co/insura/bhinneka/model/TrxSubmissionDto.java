package id.co.insura.bhinneka.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TrxSubmissionDto extends EBaseDto{

    private String spajNo ;
    private String noVa ;
    private int version ;
    private String groupPolicyId ;

    @NotNull(message = "${agentCode.cannot.be.empty}")
    private String agentCode ;

    @NotNull(message = "${agentName.cannot.be.empty}")
    private String agentName ;

    @NotNull(message = "${officeName.cannot.be.empty}")
    private String officeName ;

    @NotNull(message = "${post.data.cannot.be.empty}")
    private String json ;

}
