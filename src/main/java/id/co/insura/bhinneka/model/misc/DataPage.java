package id.co.insura.bhinneka.model.misc;

import lombok.Data;

import java.util.List;

@Data
public class DataPage<O> {
    List<O> datas ;

    int pageElement ;
    int totalPage ;
    long totalElement ;

    public DataPage(List datas, int pageElement, long totalElement){
        this.datas = datas;
        this.pageElement = pageElement;
        this.totalElement = totalElement ;
        if(totalElement==0 && totalElement==0){
            this.totalPage = 0 ;
        }else{
            this.totalPage = (int) ((totalElement / pageElement) + ((totalElement%pageElement)>0?1:0));
        }
    }

    public static DataPage builder(List datas, int pageElement, long totalElement){
        DataPage dataPage = new DataPage(datas, pageElement, totalElement);
        return dataPage ;
    }
}
