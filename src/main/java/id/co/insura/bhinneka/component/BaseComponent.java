package id.co.insura.bhinneka.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.MappedSuperclass;
import java.util.Locale;

@MappedSuperclass
public class BaseComponent {
    @Autowired
    MessageSource messageSource;


    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

}
