package id.co.insura.bhinneka.component;

import id.co.insura.bhinneka.service.esub.TrxSubmissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AppScheduler extends BaseComponent {

//      * "0 0 * * * *" = the top of every hour of every day.
//      * "*/10 * * * * *" = every ten seconds.
//      * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
//      * "0 0 8,10 * * *" = 8 and 10 o'clock of every day.
//      * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
//      * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
//      * "0 0 0 25 12 ?" = every Christmas Day at midnight

    /*
    * <second> <minute> <hour> <day-of-month> <month> <day-of-week> <year> <command>
    * */

    Logger logger = LoggerFactory.getLogger(BaseComponent.class);
    @Value("${check.pending.submission.rate.scheduler}") protected int checkPendingSubmissionRateScheduler ;
    static Date lastCheckPendingDate = null ;

    @Autowired TrxSubmissionService trxSubmissionService ;


//    @Transactional
//    @Scheduled(fixedRate = 1000*60*5)
//    public void _5minutesQuartz() {
//        logger.debug("### _5minutesQuartz() scheduler execution");
//        if(lastCheckPendingDate==null){
//            lastCheckPendingDate = new Date();
//            checkPendingSubmission();
//            return;
//        }
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(lastCheckPendingDate);
//        calendar.add(Calendar.MILLISECOND, checkPendingSubmissionRateScheduler);
//        if(calendar.getTime().before(new Date())){
//            lastCheckPendingDate = new Date();
//            checkPendingSubmission();
//            return;
//        }
//    }


//    private void checkPendingSubmission(){
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException ie) {
//            Thread.currentThread().interrupt();
//        }
//        logger.debug("### checkPendingSubmission()");
//        List<TrxSubmission> trxSubmissions = trxSubmissionService.trxSubmissions(MstStatus.PENDING_ID, "tsb.created", false);
//        trxSubmissions.forEach(trxSubmission -> {
//            trxSubmission = trxSubmissionService.submitPost(trxSubmission);
//        });
//    }




}
