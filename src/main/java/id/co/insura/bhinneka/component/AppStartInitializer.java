package id.co.insura.bhinneka.component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.co.insura.bhinneka.config.Master;
import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.service.MstStatusService;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class AppStartInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger logger = LoggerFactory.getLogger(AppStartInitializer.class);

    @Autowired MstStatusService mstStatusService ;
    @Autowired Gson gson ;
    @Autowired Utils utils ;
    HashMap<Integer, String> map = new HashMap<>();

    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        logger.debug("### BUILD MASTER DATA");
        buildMasterStatus();
        logger.debug("### FINISH BUILD MASTER DATA");
        logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
        logger.debug("############## FINISH BUILDING MASTER DATA AT "+ new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date()) +" ###################");
    }

    @Transactional
    void buildMasterStatus(){
        List<MstStatus> mstStatuses = mstStatusService.findAll();
        if(mstStatuses==null || mstStatuses.size()<=0){
            int k = 1 ;

            try{
//                String mstStatusesJson = utils.readContentFromResource(Constant.MASTER_STATUS_JSON_FILE);
//                Type mstStatusType = new TypeToken<List<MstStatus>>() {}.getType();
                List<MstStatus> mstStatusList = new ArrayList<>();
                mstStatusList.addAll(Master.statuses());
                //gson.fromJson(mstStatusesJson, mstStatusType);
                for (int i = 0; i < mstStatusList.size(); i++) {
                    MstStatus mstStatus  = mstStatusList.get(i);
                    if(!mstStatusService.existsById(mstStatus.getId())){
                        mstStatus = mstStatusService.save(mstStatus);
                        map.put(k++, "- Generate Master Status ### ID : "+String.valueOf(mstStatus.getId()));
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }






}
