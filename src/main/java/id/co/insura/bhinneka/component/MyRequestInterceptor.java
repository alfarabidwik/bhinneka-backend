package id.co.insura.bhinneka.component;

import com.google.gson.Gson;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;

@Component
public class MyRequestInterceptor extends HandlerInterceptorAdapter {

    public static final String TAG = MyRequestInterceptor.class.getName();

    private final static Logger logger = LoggerFactory.getLogger(MyRequestInterceptor.class);

    @Autowired private Gson gson ;

//	@Autowired @Lazy JwtTokenProvider jwtTokenProvider ;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        HttpServletRequest requestCacheWrapperObject = new ContentCachingRequestWrapper(request);
        Map map = requestCacheWrapperObject.getParameterMap();
        String incomingRequest = "### INCOMING REQUEST ### "+request.getRequestURI()+" :: ";
        int i = 1;
        for (String httpHeaderName : Collections.list(request.getHeaderNames())) {
            String value = request.getHeader(httpHeaderName);
            incomingRequest = incomingRequest+" >> "+i+" >> Header name : "+httpHeaderName+" >> Value : "+value;
        }
//        if(!enableSwaggerExecuteTest){
//            if(incomingRequest.contains("/swagger-ui.html") && request.getRequestURI().contains("/api")){
//                throw new AppException(Constant.FAILED_CODE, "Dilarang menggunakan swagger untuk testing, test hanya diperbolehkan menggunakan platformnya masing-masing (MobileApps, AdminApps) sehingga data yang dimasukkan lebih sistematis");
//            }
//        }

        String json = "NO_PARAM";
        try {
            if(map.containsKey("spajPdfBase64")){
                map.put("spajPdfBase64","(Base64)");
            }
            if(map.containsKey("idCardBase64")){
                map.put("idCardBase64","(Base64)");
            }
            if(map.containsKey("attachmentBase64")){
                map.put("attachmentBase64","(Base64)");
            }
            json = gson.toJson(map);
            incomingRequest = incomingRequest+" >> BODY >> "+json;
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        logger.debug(TAG+" : {}", incomingRequest);
//        String authorization = jwtTokenProvider.resolveToken(request);
//        if(authorization!=null){
//            String loginId = jwtTokenProvider.getLoginId(authorization);
//            List<String> roles = jwtTokenProvider.getRoles(authorization);
//            Long userId = jwtTokenProvider.getUserId(authorization);
//            Long employerId = jwtTokenProvider.getEmployerId(authorization);
//            Long employeeid = jwtTokenProvider.getEmployeeId(authorization);
//            authorization = jwtTokenProvider.createToken(loginId, userId, employerId, employeeid, roles);
//            response.setHeader(Constant.AUTHORIZATION, authorization);
//            logger.debug(" New Authorization for Login id : {}, With role {}, in a response {} ", loginId, roles, authorization);
//        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if(ex!=null) {
            ex.printStackTrace();
            logger.error("### CONTAINING EXCEPTION ### {}", ExceptionUtils.getRootCauseMessage(ex));
        }
        try{
            logger.debug("### STATUS ### {} with handler {}", response.getStatus(), handler);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}