package id.co.insura.bhinneka.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.co.insura.bhinneka.annotations.SwagEnable;
import id.co.insura.bhinneka.annotations.SwagPrivate;
import id.co.insura.bhinneka.component.MyRequestInterceptor;
import id.co.insura.bhinneka.utils.AppRestTemplate;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Locale;

@Configuration
@EnableSwagger2
//@EnableWebMvc
public class BasicConfiguration {


    private static final Logger logger4j = LoggerFactory.getLogger(BasicConfiguration.class);


    @Bean
    public Docket bhinnekaApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("1 - Bhinneka Documentation")
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.withMethodAnnotation(SwagEnable.class))
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Bhinneka Api Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for Bhinneka Application")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@insura.co.id"))
                                .build()
                );
    }

//    @Bean
//    public Docket privateApi() {
//        return new Docket(DocumentationType.SWAGGER_12)
//                .groupName("2 - Private Documentation")
//                .select()
//                .paths(PathSelectors.any())
//                .apis(RequestHandlerSelectors.withMethodAnnotation(SwagPrivate.class))
//                .build().apiInfo(
//                        new ApiInfoBuilder()
//                                .title("Bhinneka Api Documentation")
//                                .description("API Documentation with json structure / formatting," +
//                                        "Rest Method, e.t.c for Bhinneka Application")
//                                .license("Apache 2.0")
//                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
//                                .termsOfServiceUrl("")
//                                .version("1.0.0")
//                                .contact(new Contact("", "", "support@insura.co.id"))
//                                .build()
//                );
//    }


//    @Bean
//    UiConfiguration uiConfig() {
//        return UiConfigurationBuilder.builder()
//                .docExpansion(DocExpansion.LIST) // or DocExpansion.NONE or DocExpansion.FULL
//                .build();
//    }

//    @Configuration
//    public class SwaggerDocumentationConfig {
//        @Bean
//        public UiConfiguration tryItOutConfig() {
//            final String[] methodsWithTryItOutButton = {  };
//            return UiConfigurationBuilder.builder().supportedSubmitMethods(methodsWithTryItOutButton).build();
//        }
//    }


    @Bean
    Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(DateAppConfig.API_DATE_FORMAT);
        return gsonBuilder.create();
    }

//    @Bean
//    public MethodValidationPostProcessor methodValidationPostProcessor() {
//        return new MethodValidationPostProcessor();
//    }

//    @Bean
//    public DataSource dataSource() throws SQLException {
//        return new HikariDataSource(this);
//    }

    @Autowired
    MyRequestInterceptor requestInterceptor ;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(requestInterceptor).addPathPatterns("/**/**/**/");
            }
        };
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages/messages", "classpath:messages/notification", "classpath:messages/adminActivity", "classpath:messages/validation");
        // If true, the key of the message will be displayed if the key is not found, instead of throwing an exception
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        // The value 0 means always reload the messages to be developer friendly
        messageSource.setCacheSeconds(0);
        return messageSource;
    }


    @Bean
    public LocaleResolver localeResolver() {
        return new SmartLocaleResolver();
    }


    public class SmartLocaleResolver extends CookieLocaleResolver {

        @Override
        public Locale resolveLocale(HttpServletRequest request) {
            for (String httpHeaderName : Collections.list(request.getHeaderNames())) {
                logger4j.debug("===========>> Header name: " + httpHeaderName);
            }
            String acceptLanguage = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);
            logger4j.debug("===========>> acceptLanguage: " + acceptLanguage);
            Locale locale = super.resolveLocale(request);
            logger4j.debug("===========>> acceptLanguage locale: " + locale.getDisplayCountry());
            if (null == locale) {
                locale = getDefaultLocale();
                logger4j.debug("===========>> Default locale: " + locale.getDisplayCountry());
            }
            return locale;
        }

    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper ;
    }

    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource);
        return validatorFactoryBean;
    }

    @Bean(name = "restTemplate")
    public AppRestTemplate getRestTemplate() {
        return new AppRestTemplate();
    }




//    @Bean
//    public SessionFactory sessionFactory(EntityManagerFactory entityManagerFactory) {
//        return entityManagerFactory.unwrap(SessionFactory.class);
//    }



}
