package id.co.insura.bhinneka.config;

import id.co.insura.bhinneka.entity.MstStatus;

import java.util.ArrayList;
import java.util.List;

public class Master {


    public static final List<MstStatus> statuses(){
        List<MstStatus> mstStatuses = new ArrayList<>();

        MstStatus mstStatus = new MstStatus();
        mstStatus.setId(1l);
        mstStatus.setStatus("submitted");
        mstStatus.setLabel("Submitted");
        mstStatus.setComment("Data esubmission sudah tersubmitted ke core dan belum bayar");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(2l);
        mstStatus.setStatus("inactive.agent");
        mstStatus.setLabel("Agen tidak aktif");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response agen tidak aktif");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(3l);
        mstStatus.setStatus("idcard.rejected");
        mstStatus.setLabel("Tidak lolos validasi KTP");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response No KTP sudah terdaftar");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(4l);
        mstStatus.setStatus("incomplete.agent.form");
        mstStatus.setLabel("Data agen belum lengkap");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response Data agen belum lengkap");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(5l);
        mstStatus.setStatus("license.expired");
        mstStatus.setLabel("Lisensi kadaluarsa");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response Lisensi kadaluarsa");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(6l);
        mstStatus.setStatus("payment.invalid");
        mstStatus.setLabel("Gagal validasi pembayaran");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response Gagal validasi pembayaran");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(7l);
        mstStatus.setStatus("payment.validation");
        mstStatus.setLabel("Validasi pembayaran");
        mstStatus.setComment("Data esubmission sudah tersubmit dengan response Statusnya belum good fund (Belum diproses core)");
        mstStatuses.add(mstStatus);

        mstStatus = new MstStatus();
        mstStatus.setId(8l);
        mstStatus.setStatus("paid");
        mstStatus.setLabel("Paid");
        mstStatus.setComment("Proses pembayaran telah dilakukan");
        mstStatuses.add(mstStatus);

        return mstStatuses;

    }
}
