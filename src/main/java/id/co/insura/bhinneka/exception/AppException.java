package id.co.insura.bhinneka.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AppException extends ResponseStatusException {

    @Getter@Setter
    private String appMessage ;

    public AppException(HttpStatus httpStatus, String appMessage){
        super(httpStatus, appMessage, new Throwable(appMessage));
        this.appMessage = appMessage;
    }

    @Override
    public String getMessage() {
        return appMessage;
    }
}
