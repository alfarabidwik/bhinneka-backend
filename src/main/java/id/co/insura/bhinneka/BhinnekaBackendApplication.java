package id.co.insura.bhinneka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BhinnekaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BhinnekaBackendApplication.class, args);
	}

}
