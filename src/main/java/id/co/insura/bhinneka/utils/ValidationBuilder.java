package id.co.insura.bhinneka.utils;

import id.co.insura.bhinneka.model.WSResponse;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ValidationBuilder {

    public static WSResponse validate(Errors errors) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < errors.getAllErrors().size(); i++) {
            ObjectError objectError = errors.getAllErrors().get(i);
            strings.add(objectError.getDefaultMessage());
        }
        WSResponse wsResponse = WSResponse.instanceError(Constant.FAILED_CODE, errors.getObjectName(), strings);
        return wsResponse;
    }
}