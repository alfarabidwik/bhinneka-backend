package id.co.insura.bhinneka.utils;

import id.co.insura.bhinneka.exception.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

@Component
public class AesEncryptor {

    Logger logger = LoggerFactory.getLogger(AesEncryptor.class);

    @Value("${chicken.on.the.cage}")
    String privateKey ;
    @Value("${chicken.out.of.the.cage}")
    String publicKey ;

    @Autowired
    Utils utils ;

    @PostConstruct
    void postConstruct() throws Exception{
//        privateKey = utils.readContentFromResource(Constant.PRIVATE_FILE_KEY);
//        publicKey = utils.readContentFromResource(Constant.PUBLIC_FILE_KEY);
        privateKey = privateKey.replaceAll("\\s+","");
        publicKey = publicKey.replaceAll("\\s+","");
        logger.debug("PUBLIC KEY ### {} ", publicKey);
        logger.debug("PRIVATE KEY ### {} ", privateKey);
    }


//    public String decrypt(String value) throws Exception{
//        Cipher cipher = Cipher.getInstance("RSA");
//        cipher.init(Cipher.DECRYPT_MODE, loadPublicKey(publicKey));
//        byte[] decodedBytes = Base64.getDecoder().decode(value);
//        return new String(cipher.doFinal(decodedBytes),"UTF-8");
//    }
//
//    public String encrypt(String value) throws Exception {
//        Cipher cipher = Cipher.getInstance("RSA");
//        cipher.init(Cipher.ENCRYPT_MODE, loadPrivateKey(privateKey));
//        byte[] bytez = cipher.doFinal(value.getBytes("UTF-8"));
//        String enc = Base64.getEncoder().encodeToString(bytez);
//        return enc ;
//    }

//    public String encrypt(String value) throws Exception {
//        byte[] cipherData = value.getBytes("UTF-8");//Base64.getDecoder().decode(value);
//        byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);
//
//        MessageDigest md5 = MessageDigest.getInstance("MD5");
//        final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, publicKey.getBytes(StandardCharsets.UTF_8), md5);
//        SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
//        IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);
//
//        byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
//        Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        aesCBC.init(Cipher.ENCRYPT_MODE, key, iv);
//        byte[] decryptedData = aesCBC.doFinal(encrypted);
//        String decryptedText = new String(decryptedData, StandardCharsets.UTF_8);
//        return decryptedText ;
//    }


    public String decrypt(String value) throws AppException{
        try{
            byte[] cipherData = Base64.getDecoder().decode(value);
            byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);

            MessageDigest md5 = MessageDigest.getInstance("MD5");
            final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, publicKey.getBytes(StandardCharsets.UTF_8), md5);
            SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

            byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
            Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
            aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] decryptedData = aesCBC.doFinal(encrypted);
            String decryptedText = new String(decryptedData, StandardCharsets.UTF_8);
            return decryptedText ;
        }catch (Exception e){
            e.printStackTrace();
            throw new AppException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

//    public PublicKey loadPublicKey(String pub) throws GeneralSecurityException, IOException {
//        byte[] data = Base64.getDecoder().decode(pub.getBytes());
//        X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(data);
//        KeyFactory fact = KeyFactory.getInstance("RSA");
//        return fact.generatePublic(pubSpec);
//    }
//
//    public PrivateKey loadPrivateKey(String priv) throws GeneralSecurityException, IOException {
//        byte[] data = Base64.getDecoder().decode(priv.getBytes());
//        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(data);
//        KeyFactory fact = KeyFactory.getInstance("RSA");
//        return fact.generatePrivate(spec);
//    }

    public static byte[][] GenerateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password, MessageDigest md) {

        int digestLength = md.getDigestLength();
        int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
        byte[] generatedData = new byte[requiredLength];
        int generatedLength = 0;

        try {
            md.reset();

            // Repeat process until sufficient data has been generated
            while (generatedLength < keyLength + ivLength) {

                // Digest data (last digest if available, password data, salt if available)
                if (generatedLength > 0)
                    md.update(generatedData, generatedLength - digestLength, digestLength);
                md.update(password);
                if (salt != null)
                    md.update(salt, 0, 8);
                md.digest(generatedData, generatedLength, digestLength);

                // additional rounds
                for (int i = 1; i < iterations; i++) {
                    md.update(generatedData, generatedLength, digestLength);
                    md.digest(generatedData, generatedLength, digestLength);
                }

                generatedLength += digestLength;
            }

            // Copy key and IV into separate byte arrays
            byte[][] result = new byte[2][];
            result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
            if (ivLength > 0)
                result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

            return result;

        } catch (DigestException e) {
            e.printStackTrace();
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());

        } finally {
            // Clean out temporary data
            Arrays.fill(generatedData, (byte)0);
        }
    }

}
