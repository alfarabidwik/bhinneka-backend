package id.co.insura.bhinneka.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import id.co.insura.bhinneka.config.DateAppConfig;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateTimeSerializer extends JsonSerializer<Date> {


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);
    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateAppConfig.dateTimeFormat);

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider){
        boolean valid = false ;
        try {
            if(date==null){
                return;
            }
            String formattedDate = dateFormat.format(date);
            jsonGenerator.writeString(formattedDate);
            valid = true ;
        }catch (Exception e){
            e.printStackTrace();
            valid = false ;
        }
        if(!valid){
            try {
                if(date==null){
                    return;
                }
                String formattedDate = dateTimeFormat.format(date);
                jsonGenerator.writeString(formattedDate);
                valid = true ;
            }catch (Exception e){
                e.printStackTrace();
                valid = false ;
            }
        }

    }
}
