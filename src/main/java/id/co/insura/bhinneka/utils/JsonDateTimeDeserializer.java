package id.co.insura.bhinneka.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import id.co.insura.bhinneka.config.DateAppConfig;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateTimeDeserializer extends JsonDeserializer {
    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateAppConfig.dateTimeFormat);
        Date date = null ;
        try {
            String dateString = jsonParser.getText();
            if(StringUtils.isEmpty(dateString)){
                return null ;
            }
            date =  dateFormat.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(date==null){
            try {
                String dateString = jsonParser.getText();
                if(StringUtils.isEmpty(dateString)){
                    return null ;
                }
                date =  dateTimeFormat.parse(dateString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return date ;

    }
}
