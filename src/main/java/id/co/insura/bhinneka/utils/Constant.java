package id.co.insura.bhinneka.utils;

public class Constant {

    public static final String SWAGGER = "swagger";
    public static final Long SYSTEM = 1l;
//    public static final int CORE_SUCCESS_CODE = 200 ;
    public static final int SUCCESS_CODE = 200 ;
    public static final int FAILED_CODE = 300 ;
    public static final String SUCCESS = "success" ;
    public static final String FETCHING_SUCCESS = "fetching.success" ;
    public static final String DELETING_SUCCESS = "deleting.success" ;
    public static final String SAVING_SUCCESS = "saving.success" ;
    public static final String J_LOGIN = "J_LOGIN";

    public static final String MASTER_STATUS_JSON_FILE = "master/statuses.json";
    public static final String PUBLIC_FILE_KEY = "master/keys/public.key";
    public static final String PRIVATE_FILE_KEY = "master/keys/private.key";

    public static final String X_IBM_CLIENT_SECRET = "X-IBM-Client-Secret";
    public static final String X_IBM_CLIENT_ID = "X-IBM-Client-Id";



}
