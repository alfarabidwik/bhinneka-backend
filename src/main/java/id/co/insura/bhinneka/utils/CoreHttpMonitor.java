package id.co.insura.bhinneka.utils;

public abstract class CoreHttpMonitor {

    public abstract void onFinish(String response);
    public abstract void onError(Exception e);
}
