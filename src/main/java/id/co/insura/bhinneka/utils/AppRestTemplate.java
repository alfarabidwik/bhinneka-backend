package id.co.insura.bhinneka.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.*;

public class AppRestTemplate extends RestTemplate {

    Logger logger = LoggerFactory.getLogger(AppRestTemplate.class);

    @Override
    public <T> ResponseEntity<T> exchange(RequestEntity<?> requestEntity, Class<T> responseType) throws RestClientException {
        return super.exchange(requestEntity, responseType);
    }


    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType) throws RestClientException {
        logger.debug("Hitting third service ### {} , Method ### {} ", url, method.name());
        if(requestEntity!=null){
            if(requestEntity.getHeaders()!=null){
                requestEntity.getHeaders().entrySet().forEach(stringListEntry -> {
                    logger.debug("HIT Header name ### {} ### Header value : {} ", stringListEntry.getKey(), stringListEntry.getValue());
                });
            }
        }
        ResponseEntity responseEntity = super.exchange(url, method, requestEntity, responseType);
        logger.debug("RESPONSE ENTITY ### {} ", responseEntity);
        return responseEntity ;
    }


//    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, ParameterizedTypeReference<T> responseType, Object... uriVariables) throws RestClientException {
//        logger.debug("Hitting third service ### {} , Method ### {} ", url, method.name());
//        if(requestEntity!=null){
//            if(requestEntity.getHeaders()!=null){
//                requestEntity.getHeaders().entrySet().forEach(stringListEntry -> {
//                    logger.debug("HIT Header name ### {} ### Header value : {} ", stringListEntry.getKey(), stringListEntry.getValue());
//                });
//            }
//        }
//        ResponseEntity responseEntity = super.exchange(url, method, requestEntity, responseType, uriVariables);
//        logger.debug("RESPONSE ENTITY ### {} ", responseEntity);
//        return responseEntity ;
//    }


    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) throws RestClientException {

        logger.debug("Hitting third service ### {} , Method ### {} ", url, method.name());
        if(requestEntity!=null){
            if(requestEntity.getHeaders()!=null){
                requestEntity.getHeaders().entrySet().forEach(stringListEntry -> {
                    logger.debug("HIT Header name ### {} ### Header value : {} ", stringListEntry.getKey(), stringListEntry.getValue());
                });
            }
        }
        ResponseEntity responseEntity = super.exchange(url,method,requestEntity, responseType, uriVariables);
        logger.debug("RESPONSE ENTITY ### {} ", responseEntity);
        return responseEntity ;
    }

}
