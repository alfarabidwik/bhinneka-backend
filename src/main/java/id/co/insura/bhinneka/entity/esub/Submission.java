package id.co.insura.bhinneka.entity.esub;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.co.insura.bhinneka.config.DateAppConfig;
import id.co.insura.bhinneka.entity.EBase;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.core.esub.Esubmission;
import id.co.insura.bhinneka.utils.JsonDateDeserializer;
import id.co.insura.bhinneka.utils.JsonDateSerializer;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import jdk.nashorn.internal.runtime.JSONFunctions;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id")
public class Submission  extends EBase implements Serializable {


    String spajNo ;
    String noVa ;
    int version ;
    String groupPolicyId ;

    @Column(nullable = false)
    String agentCode ;

    @Column(nullable = false)
    String agentName ;

    @Column(nullable = false)
    String officeName ;

    @Column(columnDefinition = "VARCHAR(MAX) DEFAULT '{}'")
    String json ;

    String spajCloudUrl ;
    String idCardCloudUrl ;

    String kodeSetor ;

    int metodePembayaran ;

    Date paymentDate ;

    @Transient
    @JsonIgnore
    Esubmission esubmission ;

    @Override
    public void initTransient() {
        super.initTransient();
        try{
            Gson gson = new GsonBuilder().setDateFormat(DateAppConfig.dateTimeFormat).create();
            esubmission = gson.fromJson(json, Esubmission.class);
        }catch (Exception e){
            e.printStackTrace();
            throw new AppException(HttpStatus.BAD_REQUEST, "please.check.your.json.according.to.a.json.core.format");
        }

    }
}
