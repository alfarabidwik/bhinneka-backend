package id.co.insura.bhinneka.entity.esub;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.co.insura.bhinneka.entity.EBase;
import id.co.insura.bhinneka.entity.MstStatus;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.core.annotation.Order;

import javax.persistence.*;

@Data
@Table(name="pv_submission_status")
@Entity
@Order(4)
@ToString(of = "id", callSuper = true)
public class PVSubmissionStatus extends EBase {


    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties({"submissionStatuses", "submissionStatus"})
    @JoinColumn(name="trx_submission_id", updatable = false, nullable = false)
    TrxSubmission submission ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("submissionStatuses")
    @JoinColumn(name="mst_status_id", updatable = false, nullable = false)
    MstStatus status ;

}
