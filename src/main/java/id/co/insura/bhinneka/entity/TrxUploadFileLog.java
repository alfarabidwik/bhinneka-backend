package id.co.insura.bhinneka.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.core.annotation.Order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="trx_upload_file_log")
@Entity
@Order(1)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class TrxUploadFileLog extends EBase {

    public static final String SPAJ = "spaj";
    public static final String ID_CARD = "id_card";
    public static final String PAYMENT_RECEIPT = "payment_receipt";



    String typeFile;
    String filename ;
    String spajNo ;


    @Column(name="request_start", columnDefinition = "DATETIME", nullable = false)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date requestStart ;

    @Column(name="request_end", columnDefinition = "DATETIME", nullable = false)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date requestEnd ;

    @Column(name="core_response", columnDefinition = "NVARCHAR(4000)")
    private String coreResponse ;

    private String exception ;



}
