package id.co.insura.bhinneka.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@DynamicUpdate
@DynamicInsert
@TypeDefs({
        @TypeDef(
                typeClass = StringArrayType.class,
                defaultForType = String[].class
        ),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)

//        @TypeDef(
//                typeClass = IntArrayType.class,
//                defaultForType = int[].class
//        ),
//        @TypeDef(
//                typeClass = EnumArrayType.class,
//                defaultForType = SensorState[].class,
//                parameters = {
//                        @Parameter(
//                                name = EnumArrayType.SQL_ARRAY_TYPE,
//                                value = "sensor_state"
//                        )
//                }
//        )
})
@SelectBeforeUpdate(false)
public abstract class BasicField {

    public abstract Long getId();
    public abstract void setId(Long id);


    @Column(name="create_date", columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    @Getter@Setter
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date created ;

    @Column(name="created_by", nullable = false, columnDefinition = "BIGINT DEFAULT 1")
    @Getter@Setter
    protected Long createdBy ;

    @Column(name="updated_date", columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP", nullable = false)
    @Getter@Setter
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date updated ;

    @Column(name="updated_by", nullable = false, columnDefinition = "BIGINT DEFAULT 1")
    @Getter@Setter
    protected Long updatedBy ;

//    @Column(name="active", columnDefinition = "boolean default true")
//    @Getter@Setter
//    protected Boolean active ;

    @PreUpdate
    @PrePersist
    public void prePersist(){
        initTransient();
    }

    @PostPersist
    @PostUpdate
    public void postPersist() {
        initTransient();
    }

    @PostLoad
    public void postLoad(){
        initTransient();
    }

    public void initTransient(){
        if(createdBy==null || createdBy.equals(0l)){
            createdBy = Constant.SYSTEM;
        }
        if(updatedBy==null || updatedBy.equals(0l)){
            updatedBy = Constant.SYSTEM;
        }
        if(created==null){
            created = new Date();
        }
        if(updated==null){
            updated = new Date();
        }
    }
}
