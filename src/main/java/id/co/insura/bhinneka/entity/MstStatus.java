package id.co.insura.bhinneka.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.core.annotation.Order;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="mst_status")
@Entity
@Order(1)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class MstStatus extends EBaseManualId{

    public static final String SUBMITTED = "submitted";
    public static final String INACTIVE_AGENT = "inactive.agent";
    public static final String IDCARD_REJECTED = "idcard.rejected";
    public static final String INCOMPLETE_AGENT_CORE = "incomplete.agent.form";
    public static final String LICENSE_EXPIRED = "license.expired";
    public static final String PAYMENT_INVALID = "payment.invalid";
    public static final String PAYMENT_VALIDATION = "payment.validation";
    public static final String PAID = "paid";

    public static final Long SUBMITTED_ID = 1l;
    public static final Long INACTIVE_AGENT_ID = 2l;
    public static final Long IDCARD_REJECTED_ID = 3l;
    public static final Long INCOMPLETE_AGENT_CORE_ID = 4l;
    public static final Long LICENSE_EXPIRED_ID = 5l;
    public static final Long PAYMENT_INVALID_ID = 6l;
    public static final Long PAYMENT_VALIDATION_ID = 7l;
    public static final Long PAID_ID = 8l;


    String status ;
    String label ;
    String comment ;

}
