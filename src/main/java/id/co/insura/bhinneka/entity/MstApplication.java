package id.co.insura.bhinneka.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.core.annotation.Order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="mst_application")
@Entity
@Order(2)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class MstApplication extends EBase{
//    SELECT id, version, api, platform, app_version, status, start_date, cutoff_date, expiry_date, message, url
//    FROM LIFEPOS.dbo.MST_APPLICATION;

    @Column(name = "version")
    int version ;

    @Column(name = "api")
    int api ;

    @Column(name = "platform")
    String platform ;

    @Column(name = "app_version")
    String appVersion ;

    @Column(name = "status")
    String status ;

    @Column(name = "start_date", columnDefinition = "DATETIME")
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date startDate ;

    @Column(name = "cut_off_date", columnDefinition = "DATETIME")
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date cutOffDate ;

    @Column(name = "expiry_date", columnDefinition = "DATETIME")
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date expiryDate ;

    @Column(name = "message")
    String message ;

    @Column(name = "url")
    String url ;

}
