package id.co.insura.bhinneka.entity.esub;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.model.core.esub.PaymentData;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.core.annotation.Order;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name="trx_submission")
@Entity
@Order(3)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class TrxSubmission extends Submission{


    @OneToMany(mappedBy = "submission", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("submission")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    Set<PVSubmissionStatus> submissionStatuses = new HashSet<>();

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("submission")
    @JoinColumn(name="submission_status_id", nullable = false)
    MstStatus submissionStatus ;


    @Transient
    @JsonIgnore
    PaymentData paymentData ;

    @Override
    public void initTransient() {
        super.initTransient();

        if(paymentData!=null){
            kodeSetor = paymentData.getKodeSetor();
            metodePembayaran= paymentData.getMetodePembayaran();
            paymentDate = paymentData.getPaymentDate();
        }

    }
}
