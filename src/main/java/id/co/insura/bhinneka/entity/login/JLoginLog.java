package id.co.insura.bhinneka.entity.login;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.insura.bhinneka.utils.JsonDateTimeDeserializer;
import id.co.insura.bhinneka.utils.JsonDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.core.annotation.Order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="j_login_log")
@Entity
@Order(3)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class JLoginLog extends Login {

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";

    private String activityType ;


    @Column(name="request_start", columnDefinition = "DATETIME", nullable = false)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date requestStart ;

    @Column(name="request_end", columnDefinition = "DATETIME", nullable = false)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date requestEnd ;


    @Column(name="core_response", columnDefinition = "NVARCHAR(MAX)")
    private String coreResponse ;

    private String exception ;



}
