package id.co.insura.bhinneka.entity.login;

import id.co.insura.bhinneka.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id")
public class Login extends EBase {
    String username ;
    String platform ;
    String imei ;
    int applicationVersion ;
    String deviceInfoJson ;

}
