package id.co.insura.bhinneka.entity;

import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
@EqualsAndHashCode(of = "id")
public abstract class EBaseManualId extends BasicField implements Serializable {
    public static final Logger logger = LoggerFactory.getLogger(EBaseManualId.class);

    @Id
    protected Long id;

    @Override
    public Long getId() {
        if (id == null || id.equals(0) || id == 0) {
            this.id = null;
        }
        return id;
    }

    @Override
    public void setId(Long id) {
        if (id == null || id.equals(0) || id == 0) {
            id = null;
        }
        this.id = id;
    }

    @Override
    public void initTransient() {
        super.initTransient();
    }
}