package id.co.insura.bhinneka.entity.login;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.core.annotation.Order;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Table(name="j_login")
@Entity
@Order(3)
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class JLogin extends Login {

    private String coreLogin ;

    @Transient
    private String authorization ;


}
