package id.co.insura.bhinneka.repository.esub;

import id.co.insura.bhinneka.entity.esub.TrxSubmissionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrxSubmissionLogRepository extends JpaRepository<TrxSubmissionLog, Long> {
}
