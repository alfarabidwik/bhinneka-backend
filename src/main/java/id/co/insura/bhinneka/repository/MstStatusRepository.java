package id.co.insura.bhinneka.repository;

import id.co.insura.bhinneka.entity.MstStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MstStatusRepository extends JpaRepository<MstStatus, Long> {

}
