package id.co.insura.bhinneka.repository;

import id.co.insura.bhinneka.entity.MstApplication;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface MstApplicationRepository extends JpaRepository<MstApplication, Long> {

    @Query("SELECT mstApp FROM MstApplication mstApp")
    Optional<MstApplication> findTopByVersion(@NotNull Sort sort);
}
