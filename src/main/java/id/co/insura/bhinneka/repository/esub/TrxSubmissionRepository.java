package id.co.insura.bhinneka.repository.esub;

import id.co.insura.bhinneka.entity.esub.TrxSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrxSubmissionRepository extends JpaRepository<TrxSubmission, Long> {
}
