package id.co.insura.bhinneka.repository;

import id.co.insura.bhinneka.entity.TrxUploadFileLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadFileLogRepository extends JpaRepository<TrxUploadFileLog, Long> {
}
