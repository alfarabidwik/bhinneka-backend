package id.co.insura.bhinneka.repository.esub;

import id.co.insura.bhinneka.entity.esub.PVSubmissionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PvSubmissionStatusRepository extends JpaRepository<PVSubmissionStatus, Long> {
}
