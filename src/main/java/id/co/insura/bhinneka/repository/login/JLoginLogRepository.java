package id.co.insura.bhinneka.repository.login;

import id.co.insura.bhinneka.entity.login.JLoginLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JLoginLogRepository extends JpaRepository<JLoginLog, Long> {
}
