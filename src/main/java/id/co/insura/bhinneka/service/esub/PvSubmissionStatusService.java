package id.co.insura.bhinneka.service.esub;

import id.co.insura.bhinneka.entity.esub.PVSubmissionStatus;
import id.co.insura.bhinneka.repository.esub.PvSubmissionStatusRepository;
import id.co.insura.bhinneka.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PvSubmissionStatusService extends BaseService<PVSubmissionStatus> {

    @Autowired PvSubmissionStatusRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }


}
