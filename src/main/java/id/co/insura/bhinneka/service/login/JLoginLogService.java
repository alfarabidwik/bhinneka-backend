package id.co.insura.bhinneka.service.login;

import id.co.insura.bhinneka.entity.login.JLoginLog;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.repository.login.JLoginLogRepository;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.Date;

@Service
public class JLoginLogService extends BaseService<JLoginLog> {

    @Autowired JLoginLogRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public CoreHttpMonitor login(String username,
                                 String platform,
                                 String imei,
                                 Integer applicationVersion,
                                 Date requestStart){
        final JLoginLog jLoginLog = new JLoginLog();
        jLoginLog.setUsername(username);
        jLoginLog.setActivityType(JLoginLog.LOGIN);
        jLoginLog.setRequestStart(requestStart);
        jLoginLog.setPlatform(platform);
        jLoginLog.setImei(imei);
        jLoginLog.setApplicationVersion(applicationVersion);


        CoreHttpMonitor coreHttpMonitor = new CoreHttpMonitor() {
            @Override
            public void onFinish(String response){
                jLoginLog.setRequestEnd(new Date());
                jLoginLog.setCoreResponse(response);
                save(jLoginLog);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                jLoginLog.setRequestEnd(new Date());
                jLoginLog.setException(e.getMessage());
                save(jLoginLog);
            }
        };
        return coreHttpMonitor;
    }

}
