package id.co.insura.bhinneka.service.login;

import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.model.core.login.CoreLogin;
import id.co.insura.bhinneka.model.core.login.CoreLoginResponse;
import id.co.insura.bhinneka.repository.login.JLoginRepository;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.utils.Constant;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import id.co.insura.bhinneka.utils.AesEncryptor;
import id.co.insura.bhinneka.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientResponseException;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@Service
public class JLoginService extends BaseService<JLogin> {

    Logger logger = LoggerFactory.getLogger(JLoginLogService.class);

    @Value("${chicken.crispy.validity.in.day}") int chickenCrispyValidityInDay ;
    @Value("${login.core.api.address}") String loginCoreApiAddress ;
    @Value("${login.key}") String loginKey ;
    @Value("${ibm.client.id}") String clientId ;
    @Value("${ibm.client.secret}") String clientSecret ;

    @Autowired JLoginRepository repository ;
    @Autowired JLoginLogService jLoginLogService ;
    @Autowired
    AesEncryptor aesEncryptor;


    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public JLogin login(String username,
                            String password,
                            String platform,
                            String imei,
                            Integer applicationVersion){

        CoreLogin coreLogin = new CoreLogin();
        coreLogin.setLogin(username);
        coreLogin.setPassword(password);
        coreLogin.setExpiry(chickenCrispyValidityInDay);

        HttpEntity<Map> entity = new HttpEntity(coreLogin, Utils.headerBuilder(Arrays.asList(Constant.X_IBM_CLIENT_ID, Constant.X_IBM_CLIENT_SECRET), Arrays.asList(clientId, clientSecret)));
        CoreHttpMonitor coreHttpMonitor = jLoginLogService.login(aesEncryptor.decrypt(username), platform, imei, applicationVersion, new Date());
        try{
            ResponseEntity responseEntity = restTemplate.exchange(loginCoreApiAddress, HttpMethod.POST, entity, String.class);
            if(responseEntity.getStatusCode().value()==HttpStatus.OK.value()){
                String responseBodyString = (String) responseEntity.getBody();
                coreHttpMonitor.onFinish(responseBodyString);

//                CoreLoginResponse coreLoginResponse = gson.fromJson(responseBodyString, CoreLoginResponse.class);//(CoreLoginResponse) responseEntity.getBody();

                JLogin jLogin = new JLogin();
                jLogin.setUsername(aesEncryptor.decrypt(username));
                jLogin.setPlatform(platform);
                jLogin.setImei(imei);
                jLogin.setApplicationVersion(applicationVersion);
//                jLogin.setCoreLogin(gson.toJson(coreLoginResponse));
                jLogin.setCoreLogin(responseBodyString);
                jLogin = save(jLogin);
                return jLogin ;
            }else{
                String responseBodyString = (String) responseEntity.getBody();
                coreHttpMonitor.onFinish(responseBodyString);
                CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);//(CoreLoginResponse) responseEntity.getBody();
                throw new AppException(responseEntity.getStatusCode(),coreResponse.getMessage());
            }
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            String responseBodyString = e.getResponseBodyAsString();
            logger.debug("HttpClientErrorException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientResponseException e){
            e.printStackTrace();
            String responseBodyString = (String) e.getResponseBodyAsString();
            logger.debug("RestClientResponseException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        } catch (Exception e){
            coreHttpMonitor.onError(e);
            e.printStackTrace();
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator"));
        }

    }

}
