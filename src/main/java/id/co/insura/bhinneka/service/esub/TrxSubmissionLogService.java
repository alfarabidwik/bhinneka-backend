package id.co.insura.bhinneka.service.esub;

import id.co.insura.bhinneka.entity.esub.TrxSubmission;
import id.co.insura.bhinneka.entity.esub.TrxSubmissionLog;
import id.co.insura.bhinneka.repository.esub.TrxSubmissionLogRepository;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TrxSubmissionLogService extends BaseService<TrxSubmissionLog> {

    @Autowired
    TrxSubmissionLogRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public CoreHttpMonitor submit(String type, TrxSubmission trxSubmission){
        TrxSubmissionLog trxSubmissionLog = mapper.map(trxSubmission, TrxSubmissionLog.class);
        trxSubmissionLog.setActivityType(type);
        trxSubmissionLog.setRequestStart(new Date());



        CoreHttpMonitor coreHttpMonitor = new CoreHttpMonitor() {
            @Override
            public void onFinish(String response){
                trxSubmissionLog.setRequestEnd(new Date());
                trxSubmissionLog.setCoreResponse(response);
                save(trxSubmissionLog);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                trxSubmissionLog.setRequestEnd(new Date());
                trxSubmissionLog.setException(e.getMessage());
                save(trxSubmissionLog);
            }
        };
        return coreHttpMonitor;
    }



}
