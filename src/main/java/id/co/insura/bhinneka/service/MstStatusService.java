package id.co.insura.bhinneka.service;

import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.repository.MstStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class MstStatusService extends BaseService<MstStatus> {

    @Autowired MstStatusRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }


    public boolean existsById(Long aLong){
        return repository.existsById(aLong);
    }


}
