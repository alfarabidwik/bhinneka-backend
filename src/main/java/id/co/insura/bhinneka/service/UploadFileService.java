package id.co.insura.bhinneka.service;

import id.co.insura.bhinneka.entity.esub.TrxSubmissionLog;
import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.model.core.esub.UploadFileResponse;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import id.co.insura.bhinneka.utils.Utils;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.lang.reflect.Type;
import java.nio.file.Files;

@Service
public class UploadFileService extends BaseService{

    Logger logger = LoggerFactory.getLogger(UploadFileService.class);

    @Autowired UploadFileLogService uploadFileLogService ;

    @Override
    public JpaRepository repository() {
        return null;
    }


    public ResponseEntity<CoreResponse<UploadFileResponse>> uploadFile(String uploadType, String spajNo, JLogin jLogin, File file) throws Exception{
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(file.getName())
                .build();
        multiValueMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        HttpEntity<byte[]> fileEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), multiValueMap);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);
        body.add("container", azureCloudContainer);
        body.add("subFolder", jLogin.getUsername());
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity(body, Utils.headerBuilder(MediaType.MULTIPART_FORM_DATA, null, null));

        Type type = new TypeToken<CoreResponse<UploadFileResponse>>(){}.getType();
        CoreHttpMonitor coreHttpMonitor = uploadFileLogService.upload(uploadType, spajNo, file.getName());
        try{
            ResponseEntity<CoreResponse<UploadFileResponse>> responseEntity = restTemplate.exchange(uploadBlobCoreAddress, HttpMethod.POST, entity, ParameterizedTypeReference.forType(type));
            logger.debug("RESPONSE STATUS = {}, BODY = {},  ", responseEntity.getStatusCode(), gson.toJson(responseEntity.getBody()));
            CoreResponse coreResponse = responseEntity.getBody();
            coreHttpMonitor.onFinish(gson.toJson(coreResponse));
            if(responseEntity.getStatusCode().value()== HttpStatus.OK.value()){
                if(coreResponse!=null){
                    return responseEntity;
                }else{
                    throw new AppException(responseEntity.getStatusCode(), message("response.status.from", String.valueOf(responseEntity.getStatusCode().value()), uploadBlobCoreAddress, "Upload file = "+file.getName()));
                }
            }else{
                if(coreResponse!=null){
                    throw new AppException(responseEntity.getStatusCode(), coreResponse.getMessage());
                }else{
                    throw new AppException(responseEntity.getStatusCode(), message("response.status.from", String.valueOf(responseEntity.getStatusCode().value()), uploadBlobCoreAddress, "Upload file = "+file.getName()));
                }
            }
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            String responseBodyString = e.getResponseBodyAsString();
            logger.debug("HttpClientErrorException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientResponseException e){
            e.printStackTrace();
            String responseBodyString = (String) e.getResponseBodyAsString();
            logger.debug("RestClientResponseException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientException e){
            coreHttpMonitor.onError(e);
            e.printStackTrace();
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("upload.file.failed.please.try.again.later", file.getName()));
        }catch (Exception e){
            coreHttpMonitor.onError(e);
            e.printStackTrace();
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("upload.file.failed.please.try.again.later", file.getName()));
        }
    }

}
