package id.co.insura.bhinneka.service;

import id.co.insura.bhinneka.entity.TrxUploadFileLog;
import id.co.insura.bhinneka.repository.UploadFileLogRepository;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UploadFileLogService extends BaseService<TrxUploadFileLog> {

    @Autowired UploadFileLogRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public CoreHttpMonitor upload(String type, String spajNo, String filename){
        final TrxUploadFileLog trxUploadFileLog = new TrxUploadFileLog();
        trxUploadFileLog.setTypeFile(type);
        trxUploadFileLog.setFilename(filename);
        trxUploadFileLog.setSpajNo(spajNo);
        trxUploadFileLog.setRequestStart(new Date());

        CoreHttpMonitor coreHttpMonitor = new CoreHttpMonitor() {
            @Override
            public void onFinish(String response){
                trxUploadFileLog.setRequestEnd(new Date());
                trxUploadFileLog.setCoreResponse(response);
                save(trxUploadFileLog);
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                trxUploadFileLog.setRequestEnd(new Date());
                trxUploadFileLog.setException(e.getMessage());
                save(trxUploadFileLog);
            }
        };
        return coreHttpMonitor;
    }

}
