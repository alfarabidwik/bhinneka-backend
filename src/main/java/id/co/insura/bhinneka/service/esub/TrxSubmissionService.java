package id.co.insura.bhinneka.service.esub;

import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.entity.TrxUploadFileLog;
import id.co.insura.bhinneka.entity.esub.PVSubmissionStatus;
import id.co.insura.bhinneka.entity.esub.TrxSubmission;
import id.co.insura.bhinneka.entity.esub.TrxSubmissionLog;
import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.model.core.esub.PaymentData;
import id.co.insura.bhinneka.model.core.esub.UploadFileResponse;
import id.co.insura.bhinneka.model.misc.DataPage;
import id.co.insura.bhinneka.repository.esub.TrxSubmissionRepository;
import id.co.insura.bhinneka.service.BaseService;
import id.co.insura.bhinneka.service.UploadFileService;
import id.co.insura.bhinneka.utils.CoreHttpMonitor;
import id.co.insura.bhinneka.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.Query;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TrxSubmissionService extends BaseService<TrxSubmission> {

    Logger logger = LoggerFactory.getLogger(TrxSubmissionService.class);

    @Value("${submission.core.api.address}")
    String submissionCoreApiAddress ;
    @Value("${submission.payment.core.api.address}")
    String submissionPaymentCoreApiAddress ;

    @Autowired TrxSubmissionRepository repository ;
    @Autowired PvSubmissionStatusService pvSubmissionStatusService ;
    @Autowired TrxSubmissionLogService trxSubmissionLogService ;
    @Autowired UploadFileService uploadFileService ;


    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public DataPage<TrxSubmission> trxSubmissionDataPage(String agentCode, String status, Boolean ascending, String sortir, Integer page){
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sql = "SELECT tsb FROM TrxSubmission tsb LEFT JOIN FETCH tsb.submissionStatus sbst WHERE 1+1 = 2 ";
        if(StringUtils.isNotEmpty(agentCode)){
            sql = sql+" AND tsb.agentCode = :agentCode ";
            paramaterMap.put("agentCode", agentCode);
        }
        if(StringUtils.isNotEmpty(status)){
            sql = sql+" AND sbst.status = :status  ";
            paramaterMap.put("status", status);
        }
        sql = sql+" ORDER BY "+sortir+" "+(ascending?" ASC ":" DESC ");
        long totalElement = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(sql);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            totalElement = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            totalElement = 0 ;
        }

        Query query = entityManager.createQuery(sql);
        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }

        int offset = page*pageRow;
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        List<TrxSubmission> trxSubmissions = query.getResultList();
        return DataPage.builder(trxSubmissions, pageRow, totalElement);
    }

    public List<TrxSubmission> trxSubmissions(Long statusId, String orderColumn, Boolean ascending){

        String sql = "SELECT tsb FROM TrxSubmission tsb LEFT JOIN FETCH tsb.submissionStatus sbst WHERE sbst.id = :statusId ";
        if(StringUtils.isNotEmpty(orderColumn) && ascending!=null){
            sql = sql+" ORDER BY "+orderColumn+" "+(ascending?" ASC ":" DESC ");
        }
        Query query = entityManager.createQuery(sql);
        query.setParameter("statusId", statusId);
        return query.getResultList();
    }


    @Transactional
    public TrxSubmission submitPost(String authorization, TrxSubmission trxSubmission, File spajPdfFile, File idCardFile) throws Exception{
        if(trxSubmission.getEsubmission()==null){
            throw new AppException(HttpStatus.BAD_REQUEST, "please.check.your.json.according.to.a.json.core.format");
        }

        trxSubmission.getEsubmission().setBlobs(new ArrayList<>());

        JLogin jLogin = jwtTokenProvider.getUser(authorization, true);
        ResponseEntity<CoreResponse<UploadFileResponse>> spajUploadResponseEntity = uploadFileService.uploadFile(TrxUploadFileLog.SPAJ, trxSubmission.getSpajNo(), jLogin, spajPdfFile);
        ResponseEntity<CoreResponse<UploadFileResponse>> idCardUploadResponseEntity = uploadFileService.uploadFile(TrxUploadFileLog.ID_CARD, trxSubmission.getSpajNo(), jLogin, idCardFile);

        UploadFileResponse spajUploadFileResponse = spajUploadResponseEntity.getBody().getData();
        trxSubmission.getEsubmission().setFileSpaj(spajUploadFileResponse.getOriginalName());
        trxSubmission.getEsubmission().getBlobs().add(spajUploadFileResponse);

        UploadFileResponse idCardUploadFileResponse = idCardUploadResponseEntity.getBody().getData();
        trxSubmission.getEsubmission().setFileKtp(idCardUploadFileResponse.getOriginalName());
        trxSubmission.getEsubmission().getBlobs().add(idCardUploadFileResponse);

        trxSubmission.getEsubmission().setNoAgen(trxSubmission.getAgentCode());

        trxSubmission = submitPost(trxSubmission);
        return trxSubmission ;
    }

    @Transactional
    public TrxSubmission submitPost(TrxSubmission trxSubmission){

        HttpEntity<Map> entity = new HttpEntity(trxSubmission.getEsubmission(), Utils.headerBuilder(null, null));

        Type type = new TypeToken<CoreResponse>(){}.getType();
        CoreHttpMonitor coreHttpMonitor = trxSubmissionLogService.submit(TrxSubmissionLog.SUBMIT, trxSubmission);
        try{
            ResponseEntity<CoreResponse> responseEntity = restTemplate.exchange(submissionCoreApiAddress, HttpMethod.POST, entity, ParameterizedTypeReference.forType(type));
            CoreResponse coreResponse = responseEntity.getBody();
            coreHttpMonitor.onFinish(gson.toJson(responseEntity.getBody()));
            if(responseEntity.getStatusCode().value()==HttpStatus.OK.value() || responseEntity.getStatusCode().value()==HttpStatus.CREATED.value()){
                MstStatus mstStatus = new MstStatus();
                mstStatus.setId(MstStatus.SUBMITTED_ID);
                trxSubmission.setSubmissionStatus(mstStatus);
                trxSubmission = save(trxSubmission);
                trxSubmission = updateStatus(trxSubmission, mstStatus.getId());
                return trxSubmission;
            }else{
                if(coreResponse!=null){
                    throw new AppException(responseEntity.getStatusCode(), coreResponse.getMessage());
                }else{
                    throw new AppException(responseEntity.getStatusCode(), message("system.error.please.contact.administrator"));
                }
            }
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            String responseBodyString = e.getResponseBodyAsString();
            logger.debug("HttpClientErrorException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientResponseException e){
            e.printStackTrace();
            String responseBodyString = (String) e.getResponseBodyAsString();
            logger.debug("RestClientResponseException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientException e){
            e.printStackTrace();
            coreHttpMonitor.onError(e);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator"));
        } catch (Exception e){
            e.printStackTrace();
            coreHttpMonitor.onError(e);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator"));
        }
    }


    @Transactional
    public TrxSubmission paymentPost(String authorization, Long esubId, Integer paymentMethod, String kodeSetor, File attachment) throws Exception{

        if(paymentMethod==PaymentData.GOPAY){
            if(StringUtils.isEmpty(kodeSetor) || attachment==null){
                throw new AppException(HttpStatus.BAD_REQUEST, "Mohon lengkapi kode pembayaran & file attachment");
            }
        }

        TrxSubmission trxSubmission = findById(esubId);
        PaymentData paymentData = new PaymentData();
        paymentData.setMetodePembayaran(paymentMethod);

        if(paymentMethod==PaymentData.GOPAY){
            JLogin jLogin = jwtTokenProvider.getUser(authorization, true);
                if(attachment!=null){
                    ResponseEntity<CoreResponse<UploadFileResponse>> paymentReceiptUploadResponseEntity = uploadFileService.uploadFile(TrxUploadFileLog.PAYMENT_RECEIPT, trxSubmission.getSpajNo(), jLogin, attachment);
                    paymentData.setBlobs(new ArrayList<>());
                    paymentData.getBlobs().add(paymentReceiptUploadResponseEntity.getBody().getData());
                }
            paymentData.setKodeSetor(kodeSetor);
            paymentData.setNoKtp(trxSubmission.getSpajNo());
            trxSubmission.setPaymentData(paymentData);
        }else{
            paymentData.setNoKtp(trxSubmission.getSpajNo());
            trxSubmission.setPaymentData(paymentData);
        }

        String paymentGson = gson.toJson(paymentData);
        Map paymentDataMap = gson.fromJson(paymentGson, new TypeToken<Map>(){}.getType());
        paymentDataMap.put("metodePembayaran", String.valueOf(paymentData.getMetodePembayaran()));
        paymentDataMap.put("metode_pembayaran", String.valueOf(paymentData.getMetodePembayaran()));

        HttpEntity<Map> entity = new HttpEntity(paymentDataMap, Utils.headerBuilder(null, null));

        Type type = new TypeToken<CoreResponse>(){}.getType();
        MstStatus mstStatus = new MstStatus();
        CoreHttpMonitor coreHttpMonitor = trxSubmissionLogService.submit(TrxSubmissionLog.PAYMENT, trxSubmission);
        try{
            ResponseEntity<CoreResponse> responseEntity = restTemplate.exchange(submissionPaymentCoreApiAddress, HttpMethod.POST, entity, ParameterizedTypeReference.forType(type));
            CoreResponse coreResponse = responseEntity.getBody();
            coreHttpMonitor.onFinish(gson.toJson(responseEntity.getBody()));
            if(responseEntity.getStatusCode().value()==HttpStatus.OK.value() || responseEntity.getStatusCode().value()==HttpStatus.CREATED.value()){
                mstStatus.setId(MstStatus.PAID_ID);
                trxSubmission.setSubmissionStatus(mstStatus);
                trxSubmission = save(trxSubmission);
                trxSubmission = updateStatus(trxSubmission, mstStatus.getId());
                return trxSubmission ;
            }else{
                if(coreResponse!=null){
                    throw new AppException(responseEntity.getStatusCode(), coreResponse.getMessage());
                }else{
                    throw new AppException(responseEntity.getStatusCode(), message("system.error.please.contact.administrator"));
                }
            }
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            String responseBodyString = e.getResponseBodyAsString();
            logger.debug("HttpClientErrorException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientResponseException e){
            e.printStackTrace();
            String responseBodyString = (String) e.getResponseBodyAsString();
            logger.debug("RestClientResponseException ### {} ",responseBodyString);
            coreHttpMonitor.onFinish(responseBodyString);
            CoreResponse coreResponse = gson.fromJson(responseBodyString, CoreResponse.class);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, coreResponse.getMessage());
        }catch (RestClientException e){
            e.printStackTrace();
            coreHttpMonitor.onError(e);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator"));
        } catch (Exception e){
            e.printStackTrace();
            coreHttpMonitor.onError(e);
            throw new AppException(HttpStatus.INTERNAL_SERVER_ERROR, message("system.error.please.contact.administrator"));
        }
    }


    public TrxSubmission updateStatus(TrxSubmission trxSubmission, Long newStatusId){
        if(trxSubmission.getId()==null){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "for.updating.status.trx.submission.id.must.not.null");
        }
        MstStatus submissionStatus = new MstStatus();
        submissionStatus.setId(newStatusId);
        trxSubmission.setSubmissionStatus(submissionStatus);

        PVSubmissionStatus pvSubmissionStatus = new PVSubmissionStatus();
        pvSubmissionStatus.setSubmission(trxSubmission);
        pvSubmissionStatus.setStatus(submissionStatus);

        pvSubmissionStatus = pvSubmissionStatusService.save(pvSubmissionStatus);
        trxSubmission = save(trxSubmission);

        return trxSubmission ;

    }

}
