package id.co.insura.bhinneka.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.co.insura.bhinneka.entity.BasicField;
import id.co.insura.bhinneka.entity.MstStatus;
import id.co.insura.bhinneka.entity.login.JLogin;
import id.co.insura.bhinneka.exception.AppException;
import id.co.insura.bhinneka.model.core.CoreResponse;
import id.co.insura.bhinneka.model.core.esub.UploadFileResponse;
import id.co.insura.bhinneka.security.JwtTokenProvider;
import id.co.insura.bhinneka.utils.AppRestTemplate;
import id.co.insura.bhinneka.utils.Utils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@MappedSuperclass
public abstract class BaseService<T extends BasicField> {

    private static final Logger logger = LoggerFactory.getLogger(BaseService.class.getName());

    @Value("${upload.blob.core.address}") String uploadBlobCoreAddress ;
    @Value("${download.blob.core.address}") String downloadBlobCoreAddress ;
    @Value("${azure.cloud.container}") String azureCloudContainer ;

    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected ModelMapper mapper;
    @Autowired
    protected AppRestTemplate restTemplate;
    @Autowired
    protected EntityManager entityManager ;

    @Autowired
    protected JwtTokenProvider jwtTokenProvider;




    @Value("${page.row}")
    protected int pageRow ;

    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();

    public List<T> findAll() {
        return repository().findAll();
    }

    public List<T> findAll(Sort sort) {
        return repository().findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return repository().findAll(pageable);
    }

    public T findById(Long id) {
        Optional<T> optionalT = repository().findById(id);
        if(!optionalT.isPresent()){
            throw new AppException(HttpStatus.UNPROCESSABLE_ENTITY, message("not.found.on.the.system", "id"));
        }
        return optionalT.get();
    }

    public void delete(Long id) {
        try{
            repository().deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            throw new AppException(HttpStatus.UNPROCESSABLE_ENTITY, "Cannot delete an object using this id, maybe this object or id is been missing on your system");
        }
    }

    @Transactional
    public T save(T object){
        object.setUpdated(new Date());
        repository().save(object);
        return object ;
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public abstract <JPA extends JpaRepository> JPA repository();


}
