package id.co.insura.bhinneka.service;

import id.co.insura.bhinneka.entity.MstApplication;
import id.co.insura.bhinneka.repository.MstApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MstApplicationService extends BaseService<MstApplication> {

    @Autowired MstApplicationRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public Optional<MstApplication> findTopByVersionDesc(){
        return repository.findTopByVersion(Sort.by(Sort.Direction.DESC, "version"));
    }



}
